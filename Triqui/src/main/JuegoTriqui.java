package main;

import java.util.Scanner;

public class JuegoTriqui {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner(System.in);
		char matrizJuego[][] = new char[3][3];
		boolean jugar=false, turno=true;
		int movimientos=0;
		System.out.println("Para jugar debes introducir la posicion en x, seguida de un espacio y la posicion en y de tu movimiento.");
		iniciarMatriz(matrizJuego);
		mostrarMatriz(matrizJuego);
		
		while(!jugar && movimientos<10) {
			
			System.out.println("Pos x, Pos y:"); int x = lector.nextInt(); int y = lector.nextInt();
			if(turno) {
				if(movimientoJug1(matrizJuego, x, y)) {
					movimientos++;
					turno=false;
				}
			}else {
				if(movimientoJug2(matrizJuego, x, y)) {
					movimientos++;
					turno=true;
				}
			}
			mostrarMatriz(matrizJuego);

			jugar = comprobarGanador(matrizJuego);
		}
		
		
	}

	public static boolean movimientoJug1(char m[][], int x, int y) {
		if(m[x][y]=='-') {
			m[x][y] = 'O'; 
			return true;
		}else {
			System.out.println("Mov Inva");
			return false;
		}
	}

	public static boolean movimientoJug2(char m[][], int x, int y) {
		if(m[x][y]=='-') {
			m[x][y] = 'X'; 
			return true;
		}else{
			System.out.println("Mov Inva");
			return false;
		}
	}
	
	public static boolean comprobarGanador(char m[][]) {
		for(int i=0; i<m.length; i++) {
			if(m[i][0]==m[i][1] && m[i][1]==m[i][2] && m[i][0]!='-') {
				System.out.println("Ganaste");
				return true;
			}
			if(m[0][i]==m[1][i] && m[1][i]==m[2][i] && m[0][i]!='-') {
				System.out.println("Ganaste");
				return true;
			}
		}
		if(m[0][0]==m[1][1] && m[1][1]==m[2][2] && m[1][1]!='-') {
			System.out.println("Ganaste");
			return true;
		}
		if(m[2][0]==m[1][1] && m[1][1]==m[0][2] && m[1][1]!='-') {
			System.out.println("Ganaste");
			return true;
		}
		return false;
	}

	public static void iniciarMatriz(char m[][]) {
		int a = 0;
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {
				m[i][j] = '-';
				a++;
			}
		}
	}

	public static void mostrarMatriz(char m[][]) {
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {
				System.out.print(" " + m[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("---------");
	}

}
